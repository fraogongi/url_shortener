from django.db import models
from django.core.urlresolvers import reverse

from .lib import to_base62, to_base10


class Link(models.Model):
    """
    Model that represents a shortened URL
    """
    url = models.URLField()
    date_submitted = models.DateTimeField(auto_now_add=True)
    
    def get_absolute_url(self):
        return reverse("link_show", kwargs={"pk": self.pk})
        
    def short_url(self):
        return reverse("redirect_short_url", kwargs={"short_url": Link.shorten(self)})
    
    @staticmethod
    def shorten(long_url):
        """ Encodes a long URL to a short url """
        link, _ = Link.objects.get_or_create(url=long_url.url)
        return str(to_base62(link.pk))
        
    @staticmethod
    def expand(slug):
        link_id = int(to_base10(slug))
        link = Link.objects.get(pk=link_id)
        return link.url
